package com.zyz.web.book.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zyz.web.book.entity.Book;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BookMapper extends BaseMapper<Book> {
    int deleteMultipleByIds(@Param("ids") List<Integer> ids);
    Book getOneBook(Integer id);
}
