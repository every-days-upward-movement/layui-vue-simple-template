package com.zyz.web.readNote.entity;

import lombok.Data;

@Data
public class ReadNotePageParam {
    private Integer currentPage;
    private Integer pageSize;
    private String bookName;
}
