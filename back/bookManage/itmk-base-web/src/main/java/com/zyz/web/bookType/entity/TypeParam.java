package com.zyz.web.bookType.entity;

import lombok.Data;

@Data
public class TypeParam {
    private Integer currentPage;
    private Integer pageSize;
    private String name;
}
