package com.zyz.web.bookType.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zyz.web.bookType.entity.BookType;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BookTypeMapper extends BaseMapper<BookType> {
    int deleteMultipleByIds(@Param("ids") List<Integer> ids);
}
