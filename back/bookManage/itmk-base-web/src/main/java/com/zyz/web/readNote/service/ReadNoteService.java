package com.zyz.web.readNote.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zyz.web.readNote.entity.ReadNote;

import java.util.List;

public interface ReadNoteService extends IService<ReadNote> {
    boolean deleteMultipleByIds(List<Integer> ids);
}
