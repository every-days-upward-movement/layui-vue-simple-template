package com.zyz.web.book.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zyz.web.book.entity.Book;

import java.util.List;

public interface BookService extends IService<Book> {
    boolean deleteMultipleByIds(List<Integer> ids);
    Book getOneBook(Integer id);
}
