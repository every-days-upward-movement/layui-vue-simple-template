package com.zyz.web.image;

import com.zyz.utils.ResultUtils;
import com.zyz.utils.ResultVo;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


@RestController
@RequestMapping("/api/upload")
public class ImageUploadController {
    //图片上传的路径
    @Value("${web.upload path}")
    private String webUploadPath;
    @PostMapping("/uploadImage")
    public ResultVo uploadImage(@RequestParam("file")MultipartFile[] file){
        List<String> imageUrls = new ArrayList<>();
        try {
            File pathDir = new File(webUploadPath);
            if (!pathDir.exists()) {
                pathDir.mkdirs(); // 创建目录
                pathDir.setWritable(true); // 设置目录可写
            }

            for (MultipartFile files : file) {
                if (!files.isEmpty()) {
                    // 获取原始文件名和扩展名
                    String originalFilename = files.getOriginalFilename();
                    String extension = originalFilename.substring(originalFilename.lastIndexOf("."));
                    // 生成新的文件名
                    String newFileName = UUID.randomUUID().toString() + extension;
                    // 构建目标文件的完整路径
                    File targetFile = new File(pathDir, newFileName);

                    // 将上传的文件保存到服务器
                    files.transferTo(targetFile);
                    // 将图片路径添加到列表
                    imageUrls.add("/图片数据/" + newFileName); // 假设静态资源访问路径为 /uploads/
                }
            }
            // 假设 ResultUtils.success() 返回一个包含成功信息和数据的 ResultVo 对象
            return ResultUtils.success("图片上传成功", imageUrls);
        } catch (IOException e) {
            // 记录异常信息，返回错误响应
            return ResultUtils.error("图片上传失败", e.getMessage());
        }
    }
}