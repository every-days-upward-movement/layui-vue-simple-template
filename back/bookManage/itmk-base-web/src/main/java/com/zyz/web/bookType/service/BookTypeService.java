package com.zyz.web.bookType.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zyz.web.bookType.entity.BookType;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BookTypeService extends IService<BookType> {
    boolean deleteMultipleByIds(List<Integer> ids);
}
