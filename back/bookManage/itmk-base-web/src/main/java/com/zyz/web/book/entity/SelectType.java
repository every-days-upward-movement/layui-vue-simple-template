package com.zyz.web.book.entity;

import lombok.Data;

@Data
public class SelectType {
    private Long value;
    private String label;
}
