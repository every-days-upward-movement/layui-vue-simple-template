package com.zyz.web.bookType.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zyz.utils.ResultUtils;
import com.zyz.utils.ResultVo;
import com.zyz.web.bookType.entity.BookType;
import com.zyz.web.bookType.entity.TypeParam;
import com.zyz.web.bookType.service.BookTypeService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/bookType")
public class BookTypeController {
    @Autowired
    private BookTypeService bookTypeService;
    //新增
    @PostMapping
    public ResultVo add(@RequestBody BookType bookType){
        if(bookTypeService.save(bookType)){
            return ResultUtils.success("添加成功！");
        }
        return ResultUtils.error("添加失败！");
    }
    //修改
    @PutMapping
    public ResultVo edit(@RequestBody BookType bookType){
        if(bookTypeService.updateById(bookType)){
            return ResultUtils.success("编辑成功！");
        }
        return ResultUtils.error("编辑失败！");
    }
    //删除
    @DeleteMapping("/{id}")
    public ResultVo delete(@PathVariable("id") Integer id){
        if(bookTypeService.removeById(id)){
            return ResultUtils.success("删除成功！");
        }
        return ResultUtils.error("删除失败！");
    }
    //列表
    @GetMapping("/list")
    public ResultVo list(TypeParam param){
        IPage<BookType> page = new Page<>(param.getCurrentPage(),param.getPageSize());
        //构造查询条件
        QueryWrapper<BookType> queryWrapper = new QueryWrapper<>();
        if(StringUtils.isNotEmpty(param.getName())){
            queryWrapper.lambda().like(BookType::getName,param.getName());
        }
        //分页查询
        IPage<BookType> typeIPage = bookTypeService.page(page,queryWrapper);
        return ResultUtils.success("查询成功！",typeIPage);
    }
    //批量删除
    @DeleteMapping("/deleteMultipleByIds")
    public ResultVo deleteMultipleByIds(@RequestParam List<Integer> ids){
        System.out.println(ids);
        if(bookTypeService.deleteMultipleByIds(ids)){
            return ResultUtils.success("批量删除成功");
        }
        return ResultUtils.error("批量删除失败");
    }
}
