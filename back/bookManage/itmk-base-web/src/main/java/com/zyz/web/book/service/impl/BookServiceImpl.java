package com.zyz.web.book.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zyz.web.book.entity.Book;
import com.zyz.web.book.mapper.BookMapper;
import com.zyz.web.book.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class BookServiceImpl extends ServiceImpl<BookMapper, Book> implements BookService {
   @Override
    public boolean deleteMultipleByIds(List<Integer> ids) {
        if(ids == null || ids.isEmpty()){
            return false;
        }
        int result = this.baseMapper.deleteMultipleByIds(ids);
        return result > 0;
    }

    @Override
    public Book getOneBook(Integer id) {
        return this.baseMapper.getOneBook(id);
    }

}
