package com.zyz.web.book.entity;

import lombok.Data;

@Data
public class BookPageParam {
    private Integer currentPage;
    private Integer pageSize;
    private String bookName;
}
