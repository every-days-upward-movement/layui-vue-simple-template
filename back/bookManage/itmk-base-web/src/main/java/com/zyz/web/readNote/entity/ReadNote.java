package com.zyz.web.readNote.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
@TableName("read_note")
public class ReadNote {
    @TableId(type = IdType.AUTO)
    private Integer id;
    @TableField("bookName")
    private String bookName;
    private String image;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date createTime;
    private String details;
}
