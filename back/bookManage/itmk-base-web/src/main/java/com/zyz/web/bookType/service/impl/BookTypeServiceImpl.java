package com.zyz.web.bookType.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zyz.web.bookType.entity.BookType;
import com.zyz.web.bookType.mapper.BookTypeMapper;
import com.zyz.web.bookType.service.BookTypeService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookTypeServiceImpl extends ServiceImpl<BookTypeMapper, BookType> implements BookTypeService {
    @Override
    public boolean deleteMultipleByIds(List<Integer> ids) {
        if(ids == null || ids.isEmpty()){
            return false;
        }
        int result = this.baseMapper.deleteMultipleByIds(ids);
        return result > 0;
    }
}
