package com.zyz.web.readNote.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zyz.utils.ResultUtils;
import com.zyz.utils.ResultVo;
import com.zyz.web.readNote.entity.ReadNote;
import com.zyz.web.readNote.entity.ReadNotePageParam;
import com.zyz.web.readNote.service.ReadNoteService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api/readNote")
public class ReadNoteController {
    @Autowired
    private ReadNoteService readNoteService;
    //新增
    @PostMapping
    public ResultVo add(@RequestBody ReadNote readNote){
        readNote.setCreateTime(new Date());
        if(readNoteService.save(readNote)){
            return ResultUtils.success("新增成功!");
        }
        return ResultUtils.error("新增失败!");
    }
    //编辑
    @PutMapping
    public ResultVo edit(@RequestBody ReadNote readNote){
        if(readNoteService.updateById(readNote)){
            return ResultUtils.success("修改成功!");
        }
        return ResultUtils.error("修改失败!");
    }
    //删除
    @DeleteMapping("/{id}")
    public ResultVo delete(@PathVariable("id") Integer id){
        if(readNoteService.removeById(id)){
            return ResultUtils.success("删除成功!");
        }
        return ResultUtils.error("删除失败!");
    }
    //列表
    @GetMapping("/list")
    public ResultVo list(ReadNotePageParam param){
        //构造分页参数
        IPage<ReadNote> page = new Page<>(param.getCurrentPage(), param.getPageSize());
        //构造查询条件
        QueryWrapper<ReadNote> query = new QueryWrapper<>();
        query.lambda().like(StringUtils.isNotEmpty(param.getBookName()),ReadNote::getBookName,param.getBookName()).orderByAsc(ReadNote::getCreateTime);
        IPage<ReadNote> list = readNoteService.page(page,query);
        return ResultUtils.success("查询成功",list);
    }
    //批量删除
    @DeleteMapping("/deleteMultipleByIds")
    public ResultVo deleteMultipleByIds(@RequestParam List<Integer> ids){
        System.out.println(ids);
        if(readNoteService.deleteMultipleByIds(ids)){
            return ResultUtils.success("批量删除成功");
        }
        return ResultUtils.error("批量删除失败");
    }
}
