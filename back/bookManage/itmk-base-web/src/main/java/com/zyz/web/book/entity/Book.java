package com.zyz.web.book.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
@TableName("t_book")
public class Book {
    @TableId(type = IdType.AUTO)
    private Integer id;
    @TableField("bookImg")
    private String bookImg;
    @TableField("bookName")
    private String bookName;
    @TableField("author")
    private String author;
    @TableField("publisher")
    private String publisher;
    @TableField("price")
    private Double price;
    @TableField("publicationDate")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date publicationDate;
    @TableField("isbncode")
    private String isbncode;
    @TableField("type")
    private String type;
}
