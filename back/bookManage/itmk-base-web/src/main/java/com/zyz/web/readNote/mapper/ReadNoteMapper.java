package com.zyz.web.readNote.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zyz.web.readNote.entity.ReadNote;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ReadNoteMapper extends BaseMapper<ReadNote> {
    int deleteMultipleByIds(@Param("ids") List<Integer> ids);
}
