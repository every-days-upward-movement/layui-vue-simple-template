package com.zyz.web.readNote.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zyz.web.readNote.entity.ReadNote;
import com.zyz.web.readNote.mapper.ReadNoteMapper;
import com.zyz.web.readNote.service.ReadNoteService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReadNoteServiceImpl extends ServiceImpl<ReadNoteMapper, ReadNote> implements ReadNoteService {
    @Override
    public boolean deleteMultipleByIds(List<Integer> ids) {
        if(ids == null || ids.isEmpty()){
            return false;
        }
        int result = this.baseMapper.deleteMultipleByIds(ids);
        return result > 0;
    }
}
