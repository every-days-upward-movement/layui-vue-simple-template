package com.zyz.utils;

public class ResultUtils {
    //返回成功
    public static ResultVo success(){
        return Vo(null,StatusCode.SUCCESS_CODE,null);
    }
    public static ResultVo success(String msg){
        return Vo(msg,StatusCode.SUCCESS_CODE,null);
    }
    public static ResultVo success(String msg,Object data){
        return Vo(msg,StatusCode.SUCCESS_CODE,data);
    }
    public static ResultVo success(String msg,Integer code){
        return Vo(msg,code,null);
    }
    public static ResultVo success(String msg,Integer code,Object data){
        return Vo(msg,code,data);
    }
    public static ResultVo Vo(String msg,Integer code,Object data){
        return new ResultVo(msg,code,data);
    }
    //返回失败
    public static ResultVo error(){
        return Vo(null,StatusCode.ERROR_CODE,null);
    }
    public static ResultVo error(String msg){
        return Vo(msg,StatusCode.ERROR_CODE,null);
    }
    public static ResultVo error(String msg,Object data){
        return Vo(msg,StatusCode.ERROR_CODE,data);
    }
    public static ResultVo error(String msg,Integer code){
        return Vo(msg,code,null);
    }
    public static ResultVo error(String msg,Integer code,Object data){
        return Vo(msg,code,data);
    }
}