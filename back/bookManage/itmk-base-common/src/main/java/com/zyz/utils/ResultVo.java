package com.zyz.utils;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ResultVo<T> {
    // 返回消息
    private String message;
    // 返回码
    private Integer code;
    // 返回数据
    private T data;

}