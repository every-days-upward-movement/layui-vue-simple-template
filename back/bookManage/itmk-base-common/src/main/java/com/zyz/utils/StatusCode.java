package com.zyz.utils;

public class StatusCode {
    //返回成功
    public static final int SUCCESS_CODE = 200;
    //错误状态码
    public static final int ERROR_CODE = 500;
    //无状态
    public static final int No_LOGIN = 600;
    public static final int NO_AUTH = 700;
}