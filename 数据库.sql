/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 8.0.11 : Database - testdb1
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`testdb1` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */;

USE `testdb1`;

/*Table structure for table `book_type` */

DROP TABLE IF EXISTS `book_type`;

CREATE TABLE `book_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '类型名称',
  `order_num` int(11) DEFAULT NULL COMMENT '排序字段',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `book_type` */

insert  into `book_type`(`id`,`name`,`order_num`) values (3,'科学类',1),(4,'历史类',2),(5,'文学类',3),(6,'艺术类',4),(7,'文化类',5),(8,'生活类',6),(9,'哲学类',7),(10,'地图地理类',8),(11,'灵感类',9),(12,'动漫幽默类',10),(13,'娱乐时尚类',11),(14,'工具书',12),(15,'理论类',13),(16,'实用类',14);

/*Table structure for table `read_note` */

DROP TABLE IF EXISTS `read_note`;

CREATE TABLE `read_note` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `bookName` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '书籍名称',
  `image` longtext COLLATE utf8_bin COMMENT '书籍图片',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `details` longtext COLLATE utf8_bin COMMENT '笔记详情',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `read_note` */

insert  into `read_note`(`id`,`bookName`,`image`,`create_time`,`details`) values (3,'测试','http://localhost:8089/图片数据/15942d57-64c2-4a13-9820-840c56fa3cc6.jpg','2024-08-26 00:18:59','<p><img src=\"http://localhost:8089/图片数据/e164a3b4-f080-4f83-911f-7d1560b20e62.jpg\" alt=\"\" data-href=\"\" style=\"\"/></p><p>2</p>');

/*Table structure for table `t_book` */

DROP TABLE IF EXISTS `t_book`;

CREATE TABLE `t_book` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `bookImg` text COMMENT '图书图片',
  `bookName` varchar(20) DEFAULT NULL COMMENT '图书名称',
  `author` varchar(20) DEFAULT NULL COMMENT '图书作者',
  `publisher` varchar(20) DEFAULT NULL COMMENT '出版社名称',
  `price` double(10,2) DEFAULT NULL COMMENT '图书价格',
  `publicationDate` datetime DEFAULT NULL COMMENT '图书出版时间',
  `isbncode` varchar(100) DEFAULT NULL COMMENT '图书ISBN码',
  `type` varchar(100) DEFAULT NULL COMMENT '类型',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

/*Data for the table `t_book` */

insert  into `t_book`(`id`,`bookImg`,`bookName`,`author`,`publisher`,`price`,`publicationDate`,`isbncode`,`type`) values (1,'http://localhost:8089/图片数据/5996b6d2-8e5b-48ac-a12b-00e0f4099b59.jpg','活着','余华','作家出版社',20.99,'2012-06-30 08:00:00','9999999','5'),(2,'http://localhost:8089/图片数据/e629dabc-23b0-41d8-aad1-3e3abe9ddcac.jpg','艾伦图灵传','安德鲁·霍奇斯','湖南科技出版社',78.00,'2013-06-30 08:00:00','999999999','3'),(3,'http://localhost:8089/图片数据/0306596f-69fe-446f-9530-2ffc40210980.jpg','百年孤独','加西亚·马尔克斯','南海出版社',48.90,'2011-06-30 08:00:00','99999999','5'),(4,'http://localhost:8089/图片数据/c6fd3f06-4017-450e-98a1-70019bfa7d09.jpg','C语言入门经典','霍尔顿','清华大学出版社',28.99,'2007-06-30 08:00:00','99999999','3'),(5,'http://localhost:8089/图片数据/85551b57-18d6-4166-bdd6-759026168db2.jpg','大数据预测','埃里克·西格尔','中信出版社',18.99,'2001-08-31 08:00:00','99999999','3'),(6,'http://localhost:8089/图片数据/a4580494-93d7-4d2a-9780-e5933bd28da8.jpg','三体','刘慈欣','重庆出版社',49.99,'2014-08-10 08:00:00','99999999','5'),(7,'http://localhost:8089/图片数据/28f9674d-6ac4-4de2-a38f-f9ac05139c28.jpg','Java编程思想','Bruce Eckel','机械工业出版社',108.00,'2010-11-02 08:00:00','99999999','15'),(8,'http://localhost:8089/图片数据/b9348542-ed28-4d6e-9d3c-2c0033adf8b8.jpg','扶桑','严歌苓','人民文学出版社',54.60,'2014-08-06 08:00:00','99999999','5'),(9,'http://localhost:8089/图片数据/1cf6f14b-a5ae-4ce6-b856-97523a44ca83.jpg','教父','马里奥·普佐','译林出版社',23.30,'2015-10-11 08:00:00','99999999','5'),(10,'http://localhost:8089/图片数据/6768bf73-92bb-4282-ae09-e2be05c493d3.jpg','看见','柴静','广西师范大学出版社',39.80,'2005-06-08 08:00:00','99999999','8'),(11,'http://localhost:8089/图片数据/b6dcc98e-6384-4b6e-9a88-979f3bd36dcd.jpg','皮囊','蔡崇达','天津人民出版社',39.80,'2003-05-01 08:00:00','99999999','5');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
