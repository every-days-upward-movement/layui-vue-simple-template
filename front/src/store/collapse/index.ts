//引入
import { defineStore } from "pinia";
//定义一个自己的store。
export const menuStore = defineStore("menuStore", {
    state: () => {
        return {
            isCollapse: false,
        };
    },
    //获取state中的数据
    getters: {
        //获取count值
        getCollapse(state) {
            return state.isCollapse;
        },
    },
    //操作state中的数据
    actions: {
        //设置值
        setCollapse() {
            this.isCollapse = !this.isCollapse;
        },
    },
});
