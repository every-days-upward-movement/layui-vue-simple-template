import { getReadNoteListApi } from "@/api/readNote";
import { ReadNoteListParam } from "@/api/readNote/readNoteModel";
import { nextTick, onMounted, reactive, ref } from "vue";

export default function useReadNoteTable() {
    //选中复选框数据的id集合
    const checkList = ref([]);
    //表格数据
    const tableList = ref([]);
    //表格高度
    const tableHeight = ref(0);
    //列表参数
    const searchParam = reactive({
        currentPage: 1,
        pageSize: 5,
        bookName: "",
        total: 0,
    });
    //列表查询
    const getList = async () => {
        let res = await getReadNoteListApi(searchParam);
        if (res && res.code == 200) {
            tableList.value = res.data.records;
            searchParam.total = res.data.total;
        }
    };
    //搜索
    const searchBtn = () => {
        getList();
    };
    //重置
    const resetBtn = () => {
        searchParam.bookName = "";
        searchParam.currentPage = 1;
        getList();
    };
    //页容量或页数改变时触发
    const pageChange = ({ currentPage, pageSize }: ReadNoteListParam) => {
        currentPage = searchParam.currentPage;
        pageSize = searchParam.pageSize;
        getList();
    };

    onMounted(() => {
        getList();
        //表格高度计算
        nextTick(() => {
            tableHeight.value = window.innerHeight - 80;
        });
    });
    return {
        searchParam,
        getList,
        searchBtn,
        resetBtn,
        checkList,
        tableList,
        tableHeight,
        pageChange,
    };
}
