import { ref } from "vue";
import { EditType, FuncList } from "@/type/BaseType";
import { ReadNoteParam } from "@/api/readNote/readNoteModel";
import useInstance from "@/hooks/useinstance";
import { deleteReadNoteApi, deleteTypeMultipleByIdsApi } from "@/api/readNote";
import { layer } from "@layui/layui-vue";
export default function useReadNote(getList: FuncList) {
    //获取全局属性
    const { global } = useInstance();
    //新增弹框的ref属性
    const addReadNoteRef = ref<{
        show: (type: string, row?: ReadNoteParam) => void;
    }>();
    //新增
    const addBtn = () => {
        addReadNoteRef.value?.show(EditType.ADD);
    };
    //编辑
    const editBtn = (row: ReadNoteParam) => {
        addReadNoteRef.value?.show(EditType.EDIT, row);
    };
    //删除
    const deleteBtn = async (row: ReadNoteParam) => {
        //信息提示
        const confirm = await global.$myconfirm("确定删除该数据吗?");
        if (confirm) {
            let res = await deleteReadNoteApi(row);
            if (res && res.code == 200) {
                layer.msg(res.message, {
                    icon: 1,
                    time: 1000,
                });
            } else {
                layer.msg(res.message, {
                    icon: 2,
                    time: 1000,
                });
            }
            //刷新表格
            getList();
        }
    };
    //批量删除
    const batchDeleteBtn = async (checkList: string[]) => {
        if (checkList.length <= 0) {
            layer.msg("请选择要删除的数据", {
                icon: 3,
                time: 1000,
            });
        } else {
            let ids = "ids=";
            const confirm = await global.$myconfirm("确定删除这些数据吗?");
            if (confirm) {
                ids += checkList.join(",");
                console.log(ids);
                let res = await deleteTypeMultipleByIdsApi(ids);
                if (res && res.code == 200) {
                    layer.msg(res.message, {
                        icon: 1,
                        time: 1000,
                    });
                } else {
                    layer.msg(res.message, {
                        icon: 2,
                        time: 1000,
                    });
                }
                getList();
            }
        }
    };
    return {
        addReadNoteRef,
        addBtn,
        editBtn,
        deleteBtn,
        batchDeleteBtn,
    };
}
