import { BookParam } from "@/api/book/BookModel";
import { EditType, FuncList } from "@/type/BaseType";
import { ref } from "vue";
import useInstance from "@/hooks/useinstance";
import { deleteBookApi, deleteMultipleByIdsApi } from "@/api/book";
import { layer } from "@layui/layui-vue";
export default function useBook(getList: FuncList) {
    //获取全局属性
    const { global } = useInstance();
    //弹框ref属性
    const addBookRef = ref<{ show: (type: string, row?: BookParam) => void }>();
    const bookInfoRef = ref<{ showInfo: (row: BookParam) => void }>();
    //查看信息
    const bookInfoBtn = (row: BookParam) => {
        bookInfoRef.value?.showInfo(row);
    };
    //新增
    const addBtn = () => {
        addBookRef.value?.show(EditType.ADD);
    };
    //编辑
    const editBtn = (row: BookParam) => {
        // console.log(row);
        addBookRef.value?.show(EditType.EDIT, row);
    };
    //删除
    const deleteBtn = async (row: BookParam) => {
        //信息提示
        const confirm = await global.$myconfirm("确定删除该数据吗?");
        if (confirm) {
            let res = await deleteBookApi(row);
            if (res && res.code == 200) {
                layer.msg(res.message, {
                    icon: 1,
                    time: 1000,
                });
            } else {
                layer.msg(res.message, {
                    icon: 2,
                    time: 1000,
                });
            }
            //刷新表格
            getList();
        }
    };
    //批量删除
    const batchDeleteBtn = async (checkList: string[]) => {
        if (checkList.length <= 0) {
            layer.msg("请选择要删除的数据", {
                icon: 3,
                time: 1000,
            });
        } else {
            let ids = "ids=";
            const confirm = await global.$myconfirm("确定删除这些数据吗?");
            if (confirm) {
                ids += checkList.join(",");
                console.log(ids);
                let res = await deleteMultipleByIdsApi(ids);
                if (res && res.code == 200) {
                    layer.msg(res.message, {
                        icon: 1,
                        time: 1000,
                    });
                } else {
                    layer.msg(res.message, {
                        icon: 2,
                        time: 1000,
                    });
                }
                getList();
            }
        }
    };
    return {
        addBookRef,
        bookInfoRef,
        bookInfoBtn,
        addBtn,
        editBtn,
        deleteBtn,
        batchDeleteBtn,
    };
}
