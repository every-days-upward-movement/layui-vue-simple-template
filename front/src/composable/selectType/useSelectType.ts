import { getBookTypeSelectApi } from "@/api/book";
import { ref } from "vue";

export default function useSelectType() {
    //接收下拉数据
    const bookTypeSelectData = ref([]);
    //获取下拉数据
    const getBookTypeSelect = async () => {
        let res = await getBookTypeSelectApi();
        if (res && res.code == 200) {
            bookTypeSelectData.value = res.data;
        }
    };
    return {
        bookTypeSelectData,
        getBookTypeSelect,
    };
}
