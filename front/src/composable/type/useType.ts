import { BookTypeParam } from "@/api/type/typeModel";
import { EditType, FuncList } from "@/type/BaseType";
import { ref } from "vue";
import useInstance from "@/hooks/useinstance";
import {
    deleteBookTypeApi,
    deleteTypeMultipleByIdsApi,
} from "@/api/type/index";
import { layer } from "@layui/layui-vue";
export default function useType(getList: FuncList) {
    //获取全局属性
    const { global } = useInstance();
    //弹框ref属性
    const addBookTypeRef = ref<{
        show: (type: string, row?: BookTypeParam) => void;
    }>();
    //新增
    const addBtn = () => {
        addBookTypeRef.value?.show(EditType.ADD);
    };
    //编辑
    const editBtn = (row: BookTypeParam) => {
        // console.log(row);
        addBookTypeRef.value?.show(EditType.EDIT, row);
    };
    //删除
    const deleteBtn = async (row: BookTypeParam) => {
        //信息提示
        const confirm = await global.$myconfirm("确定删除该数据吗?");
        if (confirm) {
            let res = await deleteBookTypeApi(row);
            if (res && res.code == 200) {
                layer.msg(res.message, {
                    icon: 1,
                    time: 1000,
                });
            } else {
                layer.msg(res.message, {
                    icon: 2,
                    time: 1000,
                });
            }
            //刷新表格
            getList();
        }
    };
    //批量删除
    const batchDeleteBtn = async (checkList: string[]) => {
        if (checkList.length <= 0) {
            layer.msg("请选择要删除的数据", {
                icon: 3,
                time: 1000,
            });
        } else {
            let ids = "ids=";
            const confirm = await global.$myconfirm("确定删除这些数据吗?");
            if (confirm) {
                ids += checkList.join(",");
                console.log(ids);
                let res = await deleteTypeMultipleByIdsApi(ids);
                if (res && res.code == 200) {
                    layer.msg(res.message, {
                        icon: 1,
                        time: 1000,
                    });
                } else {
                    layer.msg(res.message, {
                        icon: 2,
                        time: 1000,
                    });
                }
                getList();
            }
        }
    };
    return {
        addBookTypeRef,
        addBtn,
        editBtn,
        deleteBtn,
        batchDeleteBtn,
    };
}
