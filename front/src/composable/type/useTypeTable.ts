import { nextTick, onMounted, reactive, ref } from "vue";
import { getBookTypeListApi } from "@/api/type";
import { BookTypeListParam } from "@/api/type/typeModel";
export default function useTypeTable() {
    //选中复选框数据的id集合
    const checkList = ref([]);
    //表格数据
    const tableList = ref([]);
    //表格高度
    const tableHeight = ref(0);
    //搜索表单绑定的对象
    const searchParam = reactive<BookTypeListParam>({
        currentPage: 1,
        pageSize: 10,
        name: "",
        total: 0,
    });
    //获取列表数据
    const getList = async () => {
        let res = await getBookTypeListApi(searchParam);
        if (res && res.code == 200) {
            tableList.value = res.data.records;
            searchParam.total = res.data.total;
        }
    };
    //搜索按钮
    const searchBtn = () => {
        getList();
    };
    //重置按钮
    const resetBtn = () => {
        searchParam.name = "";
        searchParam.currentPage = 1;
        getList();
    };
    //页容量或页数改变时触发
    const pageChange = ({ currentPage, pageSize }: BookTypeListParam) => {
        currentPage = searchParam.currentPage;
        pageSize = searchParam.pageSize;
        getList();
    };

    onMounted(() => {
        getList();
        //表格高度计算
        nextTick(() => {
            tableHeight.value = window.innerHeight - 80;
        });
    });
    return {
        searchParam,
        searchBtn,
        resetBtn,
        getList,
        tableList,
        pageChange,
        tableHeight,
        checkList,
    };
}
