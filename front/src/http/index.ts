import axios, {
    AxiosInstance,
    AxiosRequestConfig,
    AxiosResponse,
    InternalAxiosRequestConfig,
} from "axios";
import { layer } from "@layui/layui-vue";
//axios请求配置
const config = {
    baseURL: "/api",
    timeout: 10000,
};
class Http {
    //axios实例
    private instance: AxiosInstance;
    //构造函数里面初始化
    constructor(config: AxiosRequestConfig) {
        this.instance = axios.create(config);
        //定义拦截器
        this.interceptors();
    }
    //拦截器
    private interceptors() {
        //添加拦截器
        //axios发送请求之前的处理
        this.instance.interceptors.request.use(
            (config: InternalAxiosRequestConfig) => {
                //在请求头部携带token
                //let token = sessionStorage.getItem('token');
                let token = "";
                if (token) {
                    config.headers!["token"] = token;
                    //把token放到header里面
                    //(config.headers as AxiosRequestHeaders).token = token
                }
                // console.log(config);
                return config;
            },
            (error: any) => {
                error.data = {};
                error.data.msg = "服务器异常，请联系管理员！";
                return error;
            }
        );
        //axios请求返回之后的处理
        //请求返回之后的处理
        this.instance.interceptors.response.use(
            (res: AxiosResponse) => {
                //console.log(res.data)
                if (res.data.code != 200) {
                    layer.msg(res.data.msg || "后端接口报错，请检查接口！", {
                        icon: 2,
                        time: 1000,
                    });
                    return Promise.reject(
                        res.data.msg || "后端接口报错，请检查接口！"
                    );
                } else {
                    return res.data;
                }
            },
            (error: any) => {
                console.log("进入错误");
                error.data = {};
                if (error && error.response) {
                    switch (error.response.status) {
                        case 400:
                            error.data.msg = "错误请求";
                            layer.msg(error.data.msg, { icon: 2, time: 1000 });
                            break;
                        case 401:
                            error.data.msg = "未授权，请重新登录";
                            layer.msg(error.data.msg, { icon: 2, time: 1000 });
                            break;
                        case 403:
                            error.data.msg = "拒绝访问";
                            layer.msg(error.data.msg, { icon: 2, time: 1000 });
                            break;
                        case 404:
                            error.data.msg = "请求错误，未找到该接口";
                            layer.msg(error.data.msg, { icon: 2, time: 1000 });
                            break;
                        case 405:
                            error.data.msg = "请求方法未允许";
                            layer.msg(error.data.msg, { icon: 2, time: 1000 });
                            break;
                        case 408:
                            error.data.msg = "请求超时";
                            layer.msg(error.data.msg, { icon: 2, time: 1000 });
                            break;
                        case 500:
                            error.data.msg = "后端接口报错，请检查接口";
                            layer.msg(error.data.msg, { icon: 2, time: 1000 });
                            break;
                        case 501:
                            error.data.msg = "网络未实现";
                            layer.msg(error.data.msg, { icon: 2, time: 1000 });
                            break;
                        case 502:
                            error.data.msg = "网络错误";
                            layer.msg(error.data.msg, { icon: 2, time: 1000 });
                            break;
                        case 503:
                            error.data.msg = "服务不可用";
                            layer.msg(error.data.msg, { icon: 2, time: 1000 });
                            break;
                        case 504:
                            error.data.msg = "网络超时";
                            layer.msg(error.data.msg, { icon: 2, time: 1000 });
                            break;
                        case 505:
                            error.data.msg = "http版本不支持该请求";
                            layer.msg(error.data.msg, { icon: 2, time: 1000 });
                            break;
                        default:
                            error.data.msg = `连接错误${error.response.status}`;
                            layer.msg(error.data.msg, { icon: 2, time: 1000 });
                        //break;
                    }
                } else {
                    error.data.msg = "连接到服务器失败";
                    layer.msg(error.data.msg, { icon: 2, time: 1000 });
                }
                return Promise.reject(error);
            }
        );
    }
    /*GET方法*/
    get(url: string, params?: object): Promise<any> {
        return this.instance.get(url, { params });
    }
    /*POST方法*/
    post(url: string, data?: object): Promise<any> {
        return this.instance.post(url, data);
    }
    /*Put方法*/
    put(url: string, data?: object): Promise<any> {
        return this.instance.put(url, data);
    }
    /*DELETE方法*/
    delete(url: string): Promise<any> {
        return this.instance.delete(url);
    }
    /*upload方法*/
    upload(url: string, params?: object): Promise<any> {
        return this.instance.post(url, params, {
            headers: {
                "Content-Type": "multipart/form-data",
            },
        });
    }
}

export default new Http(config);
