import http from "@/http";
/**
 * 图片上传
 * @param params
 * @returns
 */
export const uploadImageApi = (params: object) => {
    return http.upload("/api/upload/uploadImage", params);
};
