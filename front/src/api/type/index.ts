import http from "@/http";
import { BookTypeParam, BookTypeListParam } from "./typeModel";
/**
 * 新增图书类型
 * @param param
 * @returns
 */
export const addBookTypeApi = (param: BookTypeParam) => {
    return http.post("/api/bookType", param);
};
/**
 * 图书类型表格
 * @param param
 * @returns
 */
export const getBookTypeListApi = (param: BookTypeListParam) => {
    return http.get("/api/bookType/list", param);
};
/**
 * 编辑图书类型
 * @param param
 * @returns
 */
export const editBookTypeApi = (param: BookTypeParam) => {
    return http.put("/api/bookType", param);
};
/**
 * 删除图书类型
 * @param param
 * @returns
 */
export const deleteBookTypeApi = (param: BookTypeParam) => {
    return http.delete(`/api/bookType/${param.id}`);
};
/**
 * 批量删除图书类型
 * @param param
 * @returns
 */
export const deleteTypeMultipleByIdsApi = (ids: string) => {
    return http.delete(`/api/bookType/deleteMultipleByIds?` + ids);
};
