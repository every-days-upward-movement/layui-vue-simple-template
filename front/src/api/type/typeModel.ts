export type BookTypeParam = {
    id: string;
    name: string;
    orderNum: string;
};
export type BookTypeListParam = {
    currentPage: number;
    pageSize: number;
    name?: string;
    total?: number;
};
