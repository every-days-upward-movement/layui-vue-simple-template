import http from "@/http";
import { ReadNoteListParam, ReadNoteParam } from "./readNoteModel";
/**
 * 新增读书笔记
 * @param param
 * @returns
 */

export const addReadNoteApi = (param: ReadNoteParam) => {
    return http.post("/api/readNote", param);
};
/**
 * 读书笔记列表
 * @param param
 * @returns
 */
export const getReadNoteListApi = (param: ReadNoteListParam) => {
    return http.get("/api/readNote/list", param);
};
/**
 * 编辑图书笔记
 * @param param
 * @returns
 */
export const editReadNoteApi = (param: ReadNoteParam) => {
    return http.put("/api/readNote", param);
};
/**
 * 删除图书笔记
 * @param param
 * @returns
 */
export const deleteReadNoteApi = (param: ReadNoteParam) => {
    return http.delete(`/api/readNote/${param.id}`);
};
/**
 * 批量删除读书笔记
 * @param param
 * @returns
 */
export const deleteTypeMultipleByIdsApi = (ids: string) => {
    return http.delete(`/api/readNote/deleteMultipleByIds?` + ids);
};
