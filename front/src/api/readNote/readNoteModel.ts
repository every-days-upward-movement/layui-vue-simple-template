export type ReadNoteParam = {
    id?: string;
    bookName: string;
    image: string;
    createTime?: string;
    details: string;
};
export type ReadNoteListParam = {
    currentPage: number;
    pageSize: number;
    bookName?: string;
    total?: number;
};
