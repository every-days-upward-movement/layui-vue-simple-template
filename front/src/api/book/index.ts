import http from "@/http";
import { BookListParam, BookParam } from "./BookModel";
/**
 * 新增图书
 * @param param
 * @returns
 */
export const addBookApi = (param: BookParam) => {
    return http.post("/api/book", param);
};
/**
 * 图书表格
 * @param param
 * @returns
 */
export const getBookListApi = (param: BookListParam) => {
    return http.get("/api/book/list", param);
};
/**
 * 编辑图书
 * @param param
 * @returns
 */
export const editBookApi = (param: BookParam) => {
    return http.put("/api/book", param);
};
/**
 * 删除图书
 * @param param
 * @returns
 */
export const deleteBookApi = (param: BookParam) => {
    return http.delete(`/api/book/${param.id}`);
};
/**
 * 批量删除图书
 * @param param
 * @returns
 */
export const deleteMultipleByIdsApi = (ids: string) => {
    return http.delete(`/api/book/deleteMultipleByIds?` + ids);
};
/**
 * 查看图书信息
 * @param param
 * @returns
 */
export const getOneBookApi = (param: BookParam) => {
    return http.get(`/api/book/${param.id}`, param);
};
export const getBookTypeSelectApi = () => {
    return http.get("/api/book/getSelectList");
};
