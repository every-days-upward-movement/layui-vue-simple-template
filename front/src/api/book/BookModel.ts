export type BookParam = {
    id: string;
    bookImg: string;
    bookName: string;
    author: string;
    type: number;
    publisher: string;
    price: string;
    publicationDate: string;
    isbncode: string;
};
export type BookListParam = {
    currentPage: number;
    pageSize: number;
    bookName?: string;
    total?: number;
};
