import { layer } from "@layui/layui-vue";
export default function myConfirm(msg: string) {
    return new Promise((resolve, reject) => {
        const okCallback = () => {
            layer.closeAll(); // 关闭所有层
            resolve(true); // 解析Promise为true
        };

        const cancelCallback = () => {
            layer.closeAll(); // 关闭所有层
            reject(false); // 解析Promise为false
        };
        layer.confirm(msg, {
            title: "系统提示",
            btn: [
                {
                    text: "确定",
                    callback: okCallback,
                },
                {
                    text: "取消",
                    callback: cancelCallback,
                },
            ],
            btnAlign: "r",
            closeBtn: "1",
        });
    }).catch(() => {
        return false;
    });
}
