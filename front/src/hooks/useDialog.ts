import { reactive } from "vue";

export default function useDialog() {
    //弹框属性
    const dialog = reactive({
        title: "新增",
        visible: false,
        width: 630,
        height: 300,
    });
    //新增按钮点击事件
    const onShow = () => {
        dialog.visible = true;
    };
    const onClose = () => {
        dialog.visible = false;
    };
    const onConfirm = () => {
        dialog.visible = false;
    };
    return {
        dialog,
        onClose,
        onConfirm,
        onShow,
    };
}
