import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import Layout from "@/views/admin/layout/index.vue";

const routes: Array<RouteRecordRaw> = [
    {
        path: "/",
        redirect: "/home",
        component: Layout,
        children: [
            {
                path: "/home",
                name: "home",
                component: () => import("@/views/admin/home/index.vue"),
                meta: {
                    title: "首页",
                    id: "home",
                    // requireAuth: true,
                },
            },
            {
                path: "/test",
                name: "test",
                component: () => import("@/components/test.vue"),
                meta: {
                    title: "测试",
                    id: "test",
                    // requireAuth: true,
                },
            },
            {
                path: "/book",
                name: "book",
                component: () => import("@/views/admin/book/index.vue"),
                meta: {
                    title: "图书管理",
                    fatherTitle: "图书管理",
                    id: "book",
                    // requireAuth: true,
                },
            },
            {
                path: "/readNote",
                name: "readNote",
                component: () => import("@/views/admin/readNote/index.vue"),
                meta: {
                    title: "读书笔记",
                    fatherTitle: "读书笔记",
                    id: "readNote",
                    // requireAuth: true,
                },
            },
            {
                path: "/type",
                name: "type",
                component: () => import("@/views/admin/type/index.vue"),
                meta: {
                    title: "书籍类型",
                    fatherTitle: "下拉属性",
                    id: "type",
                    // requireAuth: true,
                },
            },
        ],
    },
    {
        path: "/:catchAll(.*)",
        name: "/404",
        component: () => import("@/views/404/index.vue"),
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;
