import { createApp } from "vue";
import myConfirm from "./utils/myConfirm";
import App from "./App.vue";
//引入Layui
import Layui from "@layui/layui-vue";
import "@layui/layui-vue/lib/index.css";
//layui封装表单组件Json-schema-form
import LayJsonSchemaForm from "@layui/json-schema-form";
import "@layui/json-schema-form/lib/index.css";
//引入router
import Router from "./router/index";
//引入pinia的构造函数
import { createPinia } from "pinia";
//实例化pinia
const pinia = createPinia();
import piniaPluginPersistedstate from "pinia-plugin-persistedstate";
pinia.use(piniaPluginPersistedstate);
const app = createApp(App);
//全局注册
app.config.globalProperties.$myconfirm = myConfirm;
app.use(pinia).use(Router).use(Layui).use(LayJsonSchemaForm).mount("#app");
