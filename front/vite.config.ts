import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import AutoImport from "unplugin-auto-import/vite";
import Components from "unplugin-vue-components/vite";
import { LayuiVueResolver } from "unplugin-vue-components/resolvers";
import { resolve } from "path";
// https://vitejs.dev/config/
export default defineConfig({
    plugins: [
        vue(),
        AutoImport({
            resolvers: [LayuiVueResolver()],
        }),
        Components({
            resolvers: [LayuiVueResolver()],
        }),
    ],
    server: {
        host: "0.0.0.0",
        port: 8082,
        hmr: true,
        open: true,
        proxy: {
            "/api": {
                target: "http://localhost:8089", //后端接口地址
                changeOrigin: true,
                rewrite: (path) => path.replace(/^\/api/, ""),
            },
        },
    },
    resolve: {
        alias: [
            {
                find: "@",
                replacement: resolve(__dirname, "src"),
            },
        ],
    },
    define: {
        "process.env": {
            BASE_API: "http://localhost:8089",
        },
    },
});
